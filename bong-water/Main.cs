﻿using System;
using robotManager.Helpful;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;
using System.Collections.Generic;
using System.Linq;

public class Main : ICustomClass
{
    public string name = "bong-water";
    public float Range { get { return 6.5f; } }
    bool burstMode = false;
    bool aoeMode = false;
    bool runRoutine = true;
    private bool _isRunning;

    public void Initialize()
    {
        CreateStatusFrame();
        EventsLuaWithArgs.OnEventsLuaWithArgs += delegate (LuaEventsId id, List<string> args)
        {
            #region MODIFIER_STATE_CHANGED
 //           Logging.Write(id.ToString());
            if (id.ToString() == "MODIFIER_STATE_CHANGED")
            {
                //  Possible values are LSHIFT, RSHIFT, LCTRL, RCTRL, LALT, and RALT
                string key = args[0];

                // 1 means that the the key has been pressed. 0 means that the key has been released
                int state = int.Parse(args[1]);

                if (state == 0)
                {
                    switch (key) {
                        case "LALT":
                            aoeMode = !aoeMode;
                            break;
                        case "LCTRL":
                            burstMode = !burstMode;
                            break;
                        case "LSHIFT":
                            runRoutine = !runRoutine;
                            break;
                        default:
                            Logging.Write("Unexpected Key");
                            break;
                    }
                }
                Lua.LuaDoString(StatusString(runRoutine, aoeMode, burstMode));

                Logging.Write(burstMode.ToString());
            }


            #endregion
        };
        _isRunning = true;
            Logging.Write(name + "is initialized.");
            Rotation();
    }

    public string StatusString (bool runRoutine, bool aoeMode, bool burstMode)
    {
        string status = @"dRotationFrame.text:SetText(""";
        string runRoutineString = runRoutine == true ? "|cFF00FF00 RUNNING" : "|cFFFF0000 STOPPED";
        string aoeModeString = aoeMode == true ? "|cFF00FF00AOE: ON" : "|cFFFF0000AOE: OFF";
        string burstModeString = burstMode == true ? "|cFF00FF00 Burst: ON" : "|cFFFF0000Burst: OFF";
        string newLine = "|n";

        return status + runRoutineString + newLine + aoeModeString + newLine + burstModeString + "\")";
    }

    public void Dispose()
    {
        _isRunning = false;
        wManager.Wow.Memory.WowMemory.UnlockFrame(true);
    }

    public void ShowConfiguration()
    {
        return;
    }

    public void CreateStatusFrame()
    {
        Lua.LuaDoString(@"
        if not dRotationFrame then
            dRotationFrame = CreateFrame(""Frame"")
            dRotationFrame:ClearAllPoints()
            dRotationFrame:SetBackdrop(StaticPopup1:GetBackdrop())
            dRotationFrame:SetHeight(70)
            dRotationFrame:SetWidth(100)

            dRotationFrame.text = dRotationFrame:CreateFontString(nil, ""BACKGROUND"", ""GameFontNormal"")
            dRotationFrame.text:SetAllPoints()
            dRotationFrame.text:SetText(""dRotation by Dreamful, Ready!"")
            dRotationFrame.text:SetTextColor(1, 1, 1, 6)
            dRotationFrame:SetPoint(""CENTER"", 0, -240)
            dRotationFrame:SetBackdropBorderColor(0, 0, 0, 0)

            dRotationFrame:SetMovable(true)
            dRotationFrame:EnableMouse(true)
            dRotationFrame:SetScript(""OnMouseDown"",function() dRotationFrame:StartMoving() end)
            dRotationFrame:SetScript(""OnMouseUp"",function() dRotationFrame:StopMovingOrSizing() end)

            dRotationFrame.Close = CreateFrame(""BUTTON"", nil, dRotationFrame, ""UIPanelCloseButton"")
            dRotationFrame.Close:SetWidth(15)
            dRotationFrame.Close:SetHeight(15)
            dRotationFrame.Close:SetPoint(""TOPRIGHT"", dRotationFrame, -8, -8)
            dRotationFrame.Close:SetScript(""OnClick"", function()
                dRotationFrame:Hide()
                DEFAULT_CHAT_FRAME:AddMessage(""dRotationStatusFrame |cffC41F3Bclosed |cffFFFFFFWrite /dRotation to enable again."") 	
            end)

            SLASH_WHATEVERYOURFRAMESARECALLED1=""/dRotation""
            SlashCmdList.WHATEVERYOURFRAMESARECALLED = function()
                if dRotationFrame:IsShown() then
                    dRotationFrame:Hide()
                else
                    dRotationFrame:Show()
                end
            end
        end");
    }

    /* Buffs */
    public Spell WindfuryWeapon = new Spell("Windfury Weapon");
    public Spell FlametongueWeapon = new Spell("Flametongue Weapon");
    public Spell LightningShield = new Spell("Lightning Shield");

    /* Totems */
    public Spell SearingTotem = new Spell("Searing Totem");

    /* Spells */
    public Spell CallOfElements = new Spell("Call of the Elements");
    public Spell EarthShock = new Spell("Earth Shock");
    public Spell FlameShock = new Spell("Flame Shock");
    public Spell LightningBolt = new Spell("Lightning Bolt");
    public Spell Stormstrike = new Spell("Stormstrike");
    public Spell LavaLash = new Spell("Lava Lash");
    public Spell UnleashElements = new Spell("Unleash Elements");
    public Spell ChainLightning = new Spell("Chain Lightning");
    public Spell FireNova = new Spell("Fire Nova");

    /* Cooldowns */
    public Spell Ascendance = new Spell("Ascendance");
    public Spell ElementalMastery = new Spell("Elemental Mastery");
    public Spell FeralSpirit = new Spell("Feral Spirit");
    public Spell FireElementalTotem = new Spell("Fire Elemental Totem");
    public Spell StormlashTotem = new Spell("Stormlash Totem");

    /* Rotation() */
    public void Rotation()
    {
        Logging.Write(name + ": Started.");

        while (_isRunning)
        {
            try
            {
                if (runRoutine && Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause)
                {
                  //  Buff();

                    if (Fight.InFight && wManager.Wow.ObjectManager.ObjectManager.Me.Target > 0)
                    {
                        wManagerSetting.CurrentSetting.UseLuaToMove = true;
                        //var watch = System.Diagnostics.Stopwatch.StartNew();
                        //wManager.Wow.Memory.WowMemory.LockFrame();
                        CombatRotation();
                        //wManager.Wow.Memory.WowMemory.UnlockFrame(true);
                        //Logging.WriteFight("Iteration took " + watch.ElapsedMilliseconds + "ms");
                        //Thread.Sleep(5);
                    }
                    if (wManager.Wow.Memory.WowMemory.FrameIsLocked)
                    {
                        wManager.Wow.Memory.WowMemory.UnlockFrame(true);
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteError(name + " ERROR: " + e);
            }
        }
        Logging.Write(name + ": Stopped.");
    }

    /* Buff() */
    public void Buff()
    {
        // Lightning Shield
        if (!LightningShield.HaveBuff && LightningShield.IsSpellUsable)
        {
            Lua.LuaDoString("CastSpellByName('Lightning Shield')");
            return;
        }

        // Windfury Main Hand
        if (!(Lua.LuaDoString<int>("result = GetWeaponEnchantInfo()", "result") == 1) && WindfuryWeapon.IsSpellUsable)
        {
            WindfuryWeapon.Launch();     // using spell.Launch() was required because Lua doesn't wait to see if cast succeeded
            return;
        }

        // Flametongue Off Hand
        if (!(Lua.LuaDoString<int>("_, _, _, result = GetWeaponEnchantInfo()", "result") == 1) && FlametongueWeapon.IsSpellUsable)
        {
            FlametongueWeapon.Launch();  // using spell.Launch() was required because Lua doesn't wait to see if cast succeeded
            return;
        }
}

    /* CombatRotation() */
    public void CombatRotation()
    {

        Logging.Write((ObjectManager.GetWoWUnitByEntry(2523).Count(u => u.Position.DistanceTo(ObjectManager.Target.Position) < 25).ToString()));
        // Fire Elemental Totem
        if (burstMode && FireElementalTotem.IsSpellUsable)
        {
            SpellManager.CastSpellByNameLUA("Fire Elemental Totem");
            Usefuls.WaitIsCasting();
            return;
        }

        // Elemental Mastery
        if (burstMode && ElementalMastery.IsSpellUsable)
        {
            SpellManager.CastSpellByNameLUA("Elemental Mastery");
            Usefuls.WaitIsCasting();
            return;
        }

        // Feral Spirit
        if (burstMode && FeralSpirit.IsSpellUsable)
        {
            SpellManager.CastSpellByNameLUA("Feral Spirit");
            Usefuls.WaitIsCasting();
            return;
        }

        // Ascendance
        if (burstMode && Ascendance.IsSpellUsable)
        {
            SpellManager.CastSpellByNameLUA("Ascendance");
            Usefuls.WaitIsCasting();
            return;
        }

        // Stormlash Totem
        if (burstMode && StormlashTotem.IsSpellUsable)
        {
            SpellManager.CastSpellByNameLUA("Stormlash Totem");
            Usefuls.WaitIsCasting();
            return;
        }

        // Flame Shock
        if (aoeMode && FlameShock.IsSpellUsable && FlameShock.IsDistanceGood && (!ObjectManager.Target.HaveBuff("Flame Shock") || (ObjectManager.Target.BuffTimeLeft("Flame Shock") < 15000)))
        {
            SpellManager.CastSpellByNameLUA("Flame Shock");
            Usefuls.WaitIsCasting();
            return;
        }

        // Fire Nova
        if (aoeMode && FireNova.IsSpellUsable && (ObjectManager.Target.HaveBuff("Flame Shock")))
        {
            Lua.LuaDoString("CastSpellByName('Fire Nova')");
            Usefuls.WaitIsCasting();
            return;
        }

        // Searing Totem
        if (SearingTotem.IsSpellUsable && (!ObjectManager.Me.TotemExist(TotemType.Fire) || (ObjectManager.GetWoWUnitByEntry(2523).Count(u => u.Position.DistanceTo(ObjectManager.Target.Position) < 25 && u.IsMyPet) <= 0)))
        {
            SpellManager.CastSpellByNameLUA("Searing Totem");
            Usefuls.WaitIsCasting();
            return;
        }

        // Chain Lightning - MW5
        if (aoeMode && ChainLightning.IsSpellUsable && ChainLightning.IsDistanceGood && ObjectManager.Me.BuffStack("Maelstrom Weapon") == 5)
        {
            Lua.LuaDoString("CastSpellByName('Chain Lightning')");
            Usefuls.WaitIsCasting();
            return;
        }

        // Unleash Elements
        if (UnleashElements.IsSpellUsable && UnleashElements.IsDistanceGood)
        {
            SpellManager.CastSpellByNameLUA("Unleash Elements");
            Usefuls.WaitIsCasting();
            return;
        }

        // Lightning Bolt - MW5
        if (LightningBolt.IsSpellUsable && LightningBolt.IsDistanceGood && ObjectManager.Me.BuffStack("Maelstrom Weapon") == 5)
        {
            SpellManager.CastSpellByNameLUA("Lightning Bolt");
            Usefuls.WaitIsCasting();
            return;
        }

        //Stormstrike
        if (Stormstrike.IsSpellUsable && Stormstrike.IsDistanceGood)
        {
            SpellManager.CastSpellByNameLUA("Stormstrike");
            Usefuls.WaitIsCasting();
            return;
        }

        // Flame Shock
        if (FlameShock.IsSpellUsable && FlameShock.IsDistanceGood && (!ObjectManager.Target.HaveBuff("Flame Shock") || (ObjectManager.Target.BuffTimeLeft("Flame Shock") < 8000)))
        {
            SpellManager.CastSpellByNameLUA("Flame Shock");
            Usefuls.WaitIsCasting();
            return;
        }

        // Lava Lash
        if (LavaLash.IsSpellUsable && LavaLash.IsDistanceGood)
        {
            SpellManager.CastSpellByNameLUA("Lava Lash");
            Usefuls.WaitIsCasting();
            return;
        }

        // Earth Shock
        if (EarthShock.IsSpellUsable && EarthShock.IsDistanceGood && (ObjectManager.Target.BuffTimeLeft("Flame Shock") > 8000) && ((SpellManager.GetSpellCooldownTimeLeft("Stormstrike") > 2000) && (SpellManager.GetSpellCooldownTimeLeft("Lava Lash") > 2000) && (SpellManager.GetSpellCooldownTimeLeft("Unleash Elements") > 2000)))
        {
            SpellManager.CastSpellByNameLUA("Earth Shock");
            Usefuls.WaitIsCasting();
            Logging.Write("Flame Shock cooldown:" + ObjectManager.Target.BuffTimeLeft("Flame Shock").ToString());
            Logging.Write("Lava Lash cooldown:" + ObjectManager.Target.BuffTimeLeft("Lava Lash").ToString());
            Logging.Write("Stormstrike cooldown:" + ObjectManager.Target.BuffTimeLeft("Stormstrike").ToString());
            return;
        }
    }
}

