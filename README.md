# Bong Water

wRobot Fight Class for 5.X Enhancement Shaman

Bong Water is a wRobot Fight Class for WoW 5.X Enhancement Shamans.  The class was designed on Tauri WoW running the 5.4.8 client and with 5.4.8 content and values running on the server.  It should theoretically work on any 5.X server but the rotation should be reviewed to ensure that it applies to each minor patch.  

## Usability

Bong Water was designed to be lean and fast.  There are very few customizable options and that is by design.  You must be at least level 86 to run this combat routine as there are no spell.IsKnown() checks to keep the routine performant.

## Frame Lock

Bong Water leverages frame lock to make sure each ability is used in the correct order.  In practice, the ~130ms ping to Tauri causes issues for the bot due to the delayed response of when spells have been cast.

## Toggle Keys

Bong water has three modes that are toggled by keyboard keys.  The keys and modes are as follows:

1. On/Off -- LSHIFT
2. Cooldown Mode -- LCTRL
3. AoE Mode -- LALT